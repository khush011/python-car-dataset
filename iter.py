import psutil
import time

def iters(file):
    begin = time.time()
    data = list()
    s =10
    for eachRec in file:
        if s==10:
            s = 11
            continue
        row = eachRec.split(",")
        row[-1] =int(row[-1][:-1])

        data.append(row)

    print("--------------Records--------------")
    for i in range(5):
        print(data[i])

    data_noheader = [data[i] for i in range(len(data)) if i!=0]


    distinctCompanies = set(data_noheader[i][1] for i in range(len(data_noheader)))
    #print(distinctCompanies)


    results = list()
    for i in distinctCompanies:
        oneCompany = [one for one in data_noheader if one[1] == i]
        oneCompany = sorted(oneCompany, key = lambda s:s[-1], reverse = True)
        try:
            results.append(oneCompany[1])
        except:
            results.append(oneCompany[0])
    print("------------------second highest cars-------------------")
    results = sorted(results, key=lambda s:s[1])
    for i in range(5):
        print(results[i])
    end = time.time()
    time.time()
    print("\nCPU percentage: "+ str(psutil.cpu_percent()))
    print("time took: " + str(end-begin))


