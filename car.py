import pandas as pd

import psutil
import time

def analytics(file):
    begin = time.time()
    df = pd.DataFrame(file)
    df.drop("index", axis=1, inplace=True)
    print("--------------Data Shape---------------")
    print(df.shape)
    print("--------------Data Stats---------------")
    print(df.describe())
    print("--------------Count of records not having null---------------")
    print(df.notnull().count())
    print("--------------Data Schema and first 5 recs---------------")
    print(df.head())
    print("--------------Total Distint Companies---------------")
    company = df["company_name"].drop_duplicates(keep="first")
    print(company.count())
    results = pd.DataFrame()
    for i in company:
        by_company = df[df["company_name"] == i]
        by_company = by_company.sort_values("price")
        try:
            by_company = by_company.iloc[[-2]]
        except:
            by_company = by_company.iloc[[-1]]
        results = results.append(by_company)

    print("--------------2nd highest car prices w.r.t Companies---------------")
    results = results.sort_values("company_name").reset_index(drop=True)
    print(results.head())
    end = time.time()
  #  print(df.sort_values("company_name").head(5))
    print("\nCPU percentage: "+ str(psutil.cpu_percent()))
    print("time took: " + str(end-begin))



